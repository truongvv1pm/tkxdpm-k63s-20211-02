package model;

public class Bike {
    private int id;
    private String type;
    private String brand;
    private int parkId;

    public String getStatus(){
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    private String status;

    public Bike(int id, String type, String brand, int parkId) {
        this.id = id;
        this.type = type;
        this.brand = brand;
        this.parkId = parkId;
    }

    public Bike(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getParkId() {
        return parkId;
    }

    public void setParkId(int parkId) {
        this.parkId = parkId;
    }
}
