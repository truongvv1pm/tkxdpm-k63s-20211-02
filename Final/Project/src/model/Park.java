package model;

public class Park {
    private int id;
    private String name;
    private String address;
    private int maxSlot;
    private int currentSlot;

    public Park(int id, String name, String position, int maxSlot, int currentSlot) {
        this.id = id;
        this.name = name;
        this.address = position;
        this.maxSlot = maxSlot;
        this.currentSlot = currentSlot;
    }

    public Park(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getMaxSlot() {
        return maxSlot;
    }

    public void setMaxSlot(int maxSlot) {
        this.maxSlot = maxSlot;
    }

    public int getCurrentSlot() {
        return currentSlot;
    }

    public void setCurrentSlot(int currentSlot) {
        this.currentSlot = currentSlot;
    }
}
