package views.screen.manage_park;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import controller.ParkController;
import controller.ManageParkController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Park;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.add_park_form.AddParkFormScreenHandler;
import views.screen.home.HomeParkItemHandler;
import views.screen.popup.NotificationPopupHandler;

public class ManageParkScreenHandler extends BaseScreenHandler{
	
	private BaseScreenHandler parent;

	@FXML
    public Button btnReturn;
	@FXML
	public Button btnAdd;
    @FXML
    private HBox hBox;

    public ManageParkScreenHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);
        setListeners();
    }

    public void setListeners(){
    	btnReturn.setOnMouseClicked(e -> this.getPreviousScreen().show());
        
        btnAdd.setOnMouseClicked(event -> {
        	try {
        		AddParkFormScreenHandler addParkFormScreenHandler = new AddParkFormScreenHandler(stage, Configs.ADD_PARK_FORM_SCREEN_PATH);
                addParkFormScreenHandler.setScreenTitle("Thêm bãi xe");
                addParkFormScreenHandler.setBController(new ParkController());
                addParkFormScreenHandler.setPreviousScreen(this);
                addParkFormScreenHandler.show();
        	}catch (IOException e) {
        		e.printStackTrace();
        	}
        });
    }
    
    public ManageParkController getBController(){
        return (ManageParkController) super.getBController();
    }

    /**
     * Display park
     */
    private void layoutPark(List parkList){
        ArrayList parkHandlerArrayList = (ArrayList) ((ArrayList) parkList).clone();

        hBox.getChildren().clear();

        while(!parkHandlerArrayList.isEmpty()){
            VBox vBox;
            hBox.getChildren().add(vBox = new VBox());
            vBox.setStyle("-fx-border-color: #B1BED5");
            
            while((vBox.getChildren().size() < 3) && !parkHandlerArrayList.isEmpty()){
                ManageParkItemHandler parkHandler = (ManageParkItemHandler) parkHandlerArrayList.get(0);
                vBox.getChildren().add(parkHandler.getContent());
                parkHandlerArrayList.remove(parkHandler);
            }

        }
    }

    public void initialize(){
        /*load all park*/
        List parkList = new ArrayList<>();
        this.setBController(new ManageParkController());

        List allPark = getBController().getAllPark();
        allPark.forEach(park -> {
            try{
                ManageParkItemHandler manageParkItemHandler = new ManageParkItemHandler(stage, Configs.PARK_DETAIL_SCREEN_PATH);
                manageParkItemHandler.setPark((Park) park);
                manageParkItemHandler.setParentScene(this);

                parkList.add(manageParkItemHandler);
            }catch(IOException e){
                e.printStackTrace();
            }
        });
        layoutPark(parkList);
        /**/
    }
    
    public void setParentScene(BaseScreenHandler handler){
        this.parent = handler;
    }

}
