package views.screen.add_park_form;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import controller.ParkController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

public class AddParkFormScreenHandler extends BaseScreenHandler implements Initializable {
	
	private BaseScreenHandler parent;

	@FXML
	private TextField tfName, tfAddress;
	@FXML
	private Spinner<Integer> spnCapacity;
	@FXML
	private Button btnSubmit;
	@FXML
	private Button btnReturn;
	@FXML
	private Label lblInformationIsMissing, lblSuccess, lblExistedError, lblWrongTypeError;
	
	public AddParkFormScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		spnCapacity.setValueFactory(
            new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 500, 1)
        );
		setListeners();
	}
	
	public void processAddPark() throws SQLException {
		String name = null;
		name = tfName.getText().trim();
		String address = null;
		address = tfAddress.getText().trim();
		
		int capacity = spnCapacity.getValue();
		
		lblInformationIsMissing.setVisible(name.isEmpty());
		lblInformationIsMissing.setVisible(address.isEmpty());
		
		if(!name.isEmpty() && !address.isEmpty()) {
			boolean isAdded = getBController().addPark(name, address, capacity);
			
			if (isAdded) {
				lblExistedError.setVisible(false);
				lblSuccess.setVisible(true);
				new java.util.Timer().schedule( 
			        new java.util.TimerTask() {
			            @Override
			            public void run() {
			                lblSuccess.setVisible(false);
			            }
			        }, 
			        3000 
				);
			} else {
				lblSuccess.setVisible(false);
				lblExistedError.setVisible(true);
			}
				
		}
	}

	public ParkController getBController() {
        return (ParkController) super.getBController();
    }
	
	public void setListeners() {
		
		btnSubmit.setOnMouseClicked(mouseEvent -> {
			try {
				processAddPark();
            } catch (SQLException e) {
                e.printStackTrace();
            }
		});
		btnReturn.setOnMouseClicked(e -> this.getPreviousScreen().show());
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setBController(new ParkController());
        setListeners();
	}
}
