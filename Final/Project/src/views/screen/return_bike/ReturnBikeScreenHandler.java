package views.screen.return_bike;

import controller.ParkController;
import controller.ReturnBikeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Park;
import utils.Configs;
import views.screen.BaseScreenHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class ReturnBikeScreenHandler extends BaseScreenHandler {

    @FXML
    VBox vbxAvailableParks;
    @FXML
    Button btnRefreshList;
    @FXML
    Button btnBack;

    public ReturnBikeScreenHandler(Stage stage, String screenPath) throws IOException, SQLException {
        super(stage, screenPath);
        btnBack.setOnMouseClicked(event -> {
            getHomeScreenHandler().show();
        });
        btnRefreshList.setOnMouseClicked(event -> {
            try {
                showBikeVacancy();
            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Display all available bike parks to screen
     */
    private void showBikeVacancy() throws IOException, SQLException {
        List<Park> availableParks = getBController().getAllPark();

        // Empty the list for new data
        vbxAvailableParks.getChildren().clear();

        for (Park park : availableParks) {
            ReturnParkHandler returnParkHandler = new ReturnParkHandler(stage, Configs.RETURN_PARK_SCREEN_PATH);
            returnParkHandler.setPark(park);
            returnParkHandler.setContents();
            returnParkHandler.setBController(getBController());
            returnParkHandler.setHomeScreenHandler(homeScreenHandler);
            returnParkHandler.setPreviousScreen(this);

            // Add content to display
            vbxAvailableParks.getChildren().add(returnParkHandler.getContent());
        }
    }

    public ReturnBikeController getBController() {
        return (ReturnBikeController) super.getBController();
    }
}
