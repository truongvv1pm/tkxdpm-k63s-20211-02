package views.screen.manage_bike;

import java.io.IOException;

import controller.AddBikeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Bike;
import views.screen.BaseScreenHandler;

public class BikeItemHandler extends BaseScreenHandler{
	@FXML
    private Label typeLabel, brandLabel, statusLabel;
    @FXML
    private ComboBox<Integer> hourBox, minuteBox;
    @FXML
    private Button editBtn;

    private Bike bike;
    private BaseScreenHandler parent;

    public BikeItemHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);
    }

    public void setBike(Bike bike){
        this.bike = bike;
        this.brandLabel.setText(bike.getBrand());
        this.typeLabel.setText(bike.getType());
        this.statusLabel.setText(bike.getStatus());

    }

    public void setParent(BaseScreenHandler handler){
        this.parent = handler;
    }

    public AddBikeController getBController(){
        return (AddBikeController) super.getBController();
    }
}
