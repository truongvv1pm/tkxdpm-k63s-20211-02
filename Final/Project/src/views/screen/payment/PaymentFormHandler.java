package views.screen.payment;

import controller.PaymentController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Pair;
import utils.Console;
import utils.exception.PaymentException;
import views.screen.BaseScreenHandler;

import java.io.IOException;
import java.util.function.Function;

public class PaymentFormHandler extends BaseScreenHandler{
    @FXML
    private Label contentLabel, feeLabel;
    @FXML
    private Button backBtn, confirmBtn;
    @FXML
    private TextField cvvCode, expireDate, cardHolder, cardNumber;

    private String content;

    /**
     * @param callback ((success, msg): Pair) => void
     */
    public void setCallback(Function<Pair<Boolean, String>, Void> callback){
        this.callback = callback;
    }

    private Function<Pair<Boolean, String>, Void> callback;

    private int fee;

    public PaymentFormHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);
        backBtn.setOnMouseClicked(e -> getPreviousScreen().show());
        confirmBtn.setOnMouseClicked(e -> {
            try{
                makeTransaction();
            }catch(IOException ex){
                ex.printStackTrace();
            }
        });
    }

    public void setContent(String content){
        this.content = content;
        contentLabel.setText(content);
    }

    public void setFee(int fee){
        this.fee = fee;
        feeLabel.setText(String.valueOf(fee));
    }

    private void makeTransaction() throws IOException{
        String cardNumStr = cardNumber.getText(), cardHolderStr = cardHolder.getText(), expireDateStr = expireDate.getText(), cvvCodeStr = cvvCode.getText();

        try{
            boolean success = fee > 0 ? getBController().payOrder(fee, content, cardNumStr, cardHolderStr,
                    expireDateStr, cvvCodeStr) : getBController().refund(cardNumStr, cardHolderStr, expireDateStr,
                    cvvCodeStr, -fee, content);
            if(success){
                Console.console("Giao Dịch Thành Công");
                callback.apply(new Pair<>(true, "Giao dịch thành công"));
            }
        }catch(PaymentException exception){
            callback.apply(new Pair<>(false, exception.getMessage()));
        }catch(NullPointerException ignored){

        }finally{
            /*dev purpose only*/
            if(getBController().reset(cardNumStr, cardHolderStr, expireDateStr, cvvCodeStr)){
                Console.console("DEV", "Reset balance");
            }
        }
    }

    public PaymentController getBController(){
        return (PaymentController) super.getBController();
    }

}
