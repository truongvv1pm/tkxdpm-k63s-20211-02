package views.screen.loginAdmin;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import controller.LoginForAdminController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.admin_settings.AdminSettingsScreenHandler;

public class LoginForAdminScreenHandler extends BaseScreenHandler implements Initializable {
	@FXML
    public TextField txfUsername;
    @FXML
    public PasswordField pwfPassword;
    @FXML
    public Button btnLogin;
    @FXML
    public Label lblUsernameErr;
    @FXML
    public Label lblPasswordErr;
    @FXML
    public Label lblLoginErr;

	public LoginForAdminScreenHandler(String screenPath) throws IOException {
		super(screenPath);
	}
	
	public LoginForAdminScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }
	
	public void processLogin() throws SQLException {
        String username = txfUsername.getText().trim();
        String password = pwfPassword.getText().trim();

        lblUsernameErr.setVisible(username.isEmpty());
        lblPasswordErr.setVisible(password.isEmpty());

        if (!username.isEmpty() && !password.isEmpty()) {
            boolean loginValid = getBController().checkLogin(username, password);

            if (loginValid) {
                Configs.username = username;
                try {
                    // Redirect to AdminSettings page
                    AdminSettingsScreenHandler adminSettingsScreenHandler = new AdminSettingsScreenHandler(stage, Configs.ADMIN_SETTINGS_SCREEN_PATH);
                    adminSettingsScreenHandler.setScreenTitle("Quản lý hệ thống");
                    adminSettingsScreenHandler.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                lblLoginErr.setVisible(true);
            }
        }
    }

    public LoginForAdminController getBController() {
        return (LoginForAdminController) super.getBController();
    }

    /**
     * Set listeners for javafx components
     */
    public void setListeners() {
        btnLogin.setOnMouseClicked(mouseEvent -> {
            try {
                processLogin();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setBController(new LoginForAdminController());
        setListeners();
    }

    @Override
    public void show() {
        super.show();
    }

}

