package views.screen.login;

import controller.HomeController;
import controller.LoginController;
import controller.LoginForAdminController;
import controller.RegisterController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;
import views.screen.loginAdmin.LoginForAdminScreenHandler;
import views.screen.register.RegisterScreenHandler;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Control the workflow in the login screen
 */
public class LoginScreenHandler extends BaseScreenHandler implements Initializable {

    @FXML
    public TextField txfUsername;
    @FXML
    public PasswordField pwfPassword;
    @FXML
    public Hyperlink hplRegister;
    @FXML
    public Hyperlink hplLoginForAdmin;
    @FXML
    public Button btnLogin;
    @FXML
    public Label lblUsernameErr;
    @FXML
    public Label lblPasswordErr;
    @FXML
    public Label lblLoginErr;

    public LoginScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    /**
     * Validate login info in UI and then call to checkLogin function in controller
     * If login info is valid, redirect to Home page, else display errors
     */
    public void processLogin() throws SQLException {
        String username = txfUsername.getText().trim();
        String password = pwfPassword.getText().trim();

        lblUsernameErr.setVisible(username.isEmpty());
        lblPasswordErr.setVisible(password.isEmpty());

        if (!username.isEmpty() && !password.isEmpty()) {
            boolean loginValid = getBController().checkLogin(username, password);

            if (loginValid) {
                Configs.username = username;
                try {
                    // Redirect to Home page
                    HomeScreenHandler homeScreenHandler = new HomeScreenHandler(stage, Configs.HOME_SCREEN_PATH);
                    homeScreenHandler.setScreenTitle("Trang chủ");
                    homeScreenHandler.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                lblLoginErr.setVisible(true);
            }
        }
    }

    public LoginController getBController() {
        return (LoginController) super.getBController();
    }

    /**
     * Set listeners for javafx components
     */
    public void setListeners() {
        btnLogin.setOnMouseClicked(mouseEvent -> {
            try {
                processLogin();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        hplRegister.setOnMouseClicked(mouseEvent -> {
            try {
                // Redirect to Register page
                RegisterScreenHandler registerHandler = new RegisterScreenHandler(stage, Configs.REGISTER_SCREEN_PATH);
                registerHandler.setScreenTitle("Đăng ký");
                registerHandler.setBController(new RegisterController());
                registerHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        hplLoginForAdmin.setOnMouseClicked(mouseEvent -> {
            try {
                // Redirect to LoginForAdmin page
                LoginForAdminScreenHandler loginForAdminHandler = new LoginForAdminScreenHandler(stage, Configs.LOGIN_FOR_ADMIN_SCREEN_PATH);
                loginForAdminHandler.setScreenTitle("Đăng nhập Quản trị viên");
                loginForAdminHandler.setBController(new LoginForAdminController());
                loginForAdminHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setBController(new LoginController());
        setListeners();
    }

    @Override
    public void show() {
        super.show();
    }

}
