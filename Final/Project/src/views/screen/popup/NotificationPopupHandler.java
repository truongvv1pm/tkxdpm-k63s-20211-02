package views.screen.popup;

import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import utils.Configs;
import views.screen.BaseScreenHandler;

import java.io.IOException;

public class NotificationPopupHandler extends BaseScreenHandler {

    @FXML
    ImageView imgIcon;

    @FXML
    Label lblMessage;

    public NotificationPopupHandler(Stage stage) throws IOException {
        super(stage, Configs.NOTIFICATION_POPUP_PATH);
    }

    private static NotificationPopupHandler popup(String message, String iconPath, Boolean undecorated) throws IOException {
        NotificationPopupHandler popup = new NotificationPopupHandler(new Stage());
        if (undecorated) popup.stage.initStyle(StageStyle.UNDECORATED);
        popup.lblMessage.setText(message);
        popup.setImage(iconPath);
        popup.stage.setAlwaysOnTop(true);
        return popup;
    }

    public static void success(String message) throws IOException {
        popup(message, Configs.SUCCESS_ICON_PATH, false).show(false);
    }

    public static void failure(String message) throws IOException {
        popup(message, Configs.FAILURE_ICON_PATH, false).show(false);
    }

    public void show(Boolean autoClose) {
        super.show();
        if (autoClose) close(0.8);
    }

    public void show(double time) {
        super.show();
        close(time);
    }

    public void close(double time) {
        PauseTransition delay = new PauseTransition(Duration.seconds(time));
        delay.setOnFinished(event -> stage.close());
        delay.play();
    }

    public void setImage(String path) {
        super.setImage(imgIcon, path);
    }

}
