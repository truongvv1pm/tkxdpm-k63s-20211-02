package views.screen.rent_bike;

import controller.PaymentController;
import controller.RentalController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Bike;
import utils.Configs;
import utils.Console;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;
import views.screen.park.ParkRentalDetailHandler;
import views.screen.payment.PaymentFormHandler;
import views.screen.popup.NotificationPopupHandler;

import java.io.IOException;
import java.util.Date;

public class ConfirmRentingPopupHandler extends BaseScreenHandler{
    @FXML
    private Label brandLabel, typeLabel, statusLabel, hourLabel, minLabel, feeLabel;
    @FXML
    private Button okBtn, cancelBtn;

    private static ConfirmRentingPopupHandler handler;

    private BaseScreenHandler parent;
    private Bike bike;
    private Integer duration;

    public void setBike(Bike bike){
        this.bike = bike;
    }

    public void setDuration(Integer duration){
        this.duration = duration;
    }

    public ConfirmRentingPopupHandler(Stage stage) throws IOException{
        super(stage, Configs.CONFIRM_RENTING_DIALOG);
    }

    public void setParent(BaseScreenHandler parent){
        this.parent = parent;
    }

    public static void show(Bike bike, Integer hour, Integer min, int fee, BaseScreenHandler parent) throws IOException{
        if(handler == null){
            /*display in popup - new stage*/
            handler = new ConfirmRentingPopupHandler(new Stage());
            handler.setBController(new RentalController());
            handler.setParent(parent);
            handler.cancelBtn.setOnMouseClicked(e -> handler.close());
        }else{
            handler.close();
        }

        handler.okBtn.setOnMouseClicked(e -> {
            try{
                handler.processPayment(fee, "Renting fee for bike " + bike.getId());
            }catch(IOException ex){
                ex.printStackTrace();
            }
        });

        handler.feeLabel.setText(String.valueOf(fee));
        handler.hourLabel.setText(hour.toString());
        handler.minLabel.setText(min.toString());

        handler.setDuration(hour * 60 + min);
        handler.setBike(bike);

        handler.brandLabel.setText(bike.getBrand());
        handler.typeLabel.setText(bike.getType());
        handler.statusLabel.setText(bike.getStatus());

        handler.show();
    }

    private void close(){
        stage.close();
    }

    /**
     * @param fee
     * @param content
     */
    private void processPayment(int fee, String content) throws IOException{
        PaymentFormHandler handler = new PaymentFormHandler(((ParkRentalDetailHandler) parent).getStage(),
                Configs.PAYMENT_FORM);

        /*set callback to payment form*/
        handler.setCallback(booleanStringPair -> {
            if(booleanStringPair.getKey()){
                /*Back to Home*/
                try{
                    NotificationPopupHandler.success("Thuê Xe Thành Công");
                }catch(IOException e){
                    e.printStackTrace();
                }
                /*db insertion*/
                getBController().rentBike(Configs.username, duration, new Date(), bike.getId(), fee);
                /**/
                try{
                    HomeScreenHandler homeScreenHandler = new HomeScreenHandler(
                            ((ParkRentalDetailHandler) parent).getStage(), Configs.HOME_SCREEN_PATH);
                    homeScreenHandler.setScreenTitle("Trang Chủ");
                    homeScreenHandler.show();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }else{
                try{
                    NotificationPopupHandler.failure("Thuê Xe Thất Bại\n".concat(booleanStringPair.getValue()));
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
            Console.console("Payment callback called");

            return null;
        });
        handler.setBController(new PaymentController());
        handler.setPreviousScreen(parent);
        handler.setContent(content);
        handler.setFee(fee);
        handler.show();
        close();
    }

    public RentalController getBController(){
        return (RentalController) super.getBController();
    }
}
