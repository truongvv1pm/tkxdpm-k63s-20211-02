package views.screen.home;

import controller.HomeController;
import controller.LoginController;
import controller.ReturnBikeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Park;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.login.LoginScreenHandler;
import views.screen.popup.NotificationPopupHandler;
import views.screen.return_bike.ReturnBikeScreenHandler;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HomeScreenHandler extends BaseScreenHandler {

    @FXML
    public Button btnReturnBike;
    @FXML
    private Button btnLogOut;
    @FXML
    private HBox hBox;

    public HomeScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
        setListeners();
    }

    public void setListeners() {
        btnReturnBike.setOnMouseClicked(event -> {
            returnBike();
        });

        btnLogOut.setOnMouseClicked(event -> {
            try {
                // Show Login screen
                LoginScreenHandler loginScreenHandler = new LoginScreenHandler(stage, Configs.LOGIN_SCREEN_PATH);
                loginScreenHandler.setScreenTitle("Đăng nhập");
                loginScreenHandler.setBController(new LoginController());
                loginScreenHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    private void returnBike() {
        if (!getBController().checkUserRental(Configs.username)) {
            try {
                NotificationPopupHandler.failure("Khách hàng chưa thuê xe!");
                return;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else try {
            // Show ReturnBike screen
            ReturnBikeScreenHandler returnBikeScreenHandler = new ReturnBikeScreenHandler(stage,
                    Configs.RETURN_BIKE_SCREEN_PATH);
            returnBikeScreenHandler.setScreenTitle("Trả xe");
            returnBikeScreenHandler.setBController(new ReturnBikeController());
            returnBikeScreenHandler.setHomeScreenHandler(this);
            returnBikeScreenHandler.setPreviousScreen(this);
            returnBikeScreenHandler.show();
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    public HomeController getBController() {
        return (HomeController) super.getBController();
    }

    /**
     * Display park
     */
    private void layoutPark(List parkList) {
        ArrayList parkHandlerArrayList = (ArrayList) ((ArrayList) parkList).clone();

        hBox.getChildren().clear();

        while (!parkHandlerArrayList.isEmpty()) {
            VBox vBox;
            hBox.getChildren().add(vBox = new VBox());
            vBox.setStyle("-fx-border-color: blue");

            while ((vBox.getChildren().size() < 2) && !parkHandlerArrayList.isEmpty()) {
                HomeParkItemHandler parkHandler = (HomeParkItemHandler) parkHandlerArrayList.get(0);
                vBox.getChildren().add(parkHandler.getContent());
                parkHandlerArrayList.remove(parkHandler);
            }

        }
    }

    public void initialize() {
        /*load all park*/
        List parkList = new ArrayList<>();
        this.setBController(new HomeController());

        List allPark = getBController().getAllPark();
        allPark.forEach(park -> {
            try {
                HomeParkItemHandler homeParkItemHandler = new HomeParkItemHandler(stage, Configs.PARK_HOME_ITEM);
                homeParkItemHandler.setPark((Park) park);
                homeParkItemHandler.setParentScene(this);

                parkList.add(homeParkItemHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        layoutPark(parkList);
        /**/
    }
}
