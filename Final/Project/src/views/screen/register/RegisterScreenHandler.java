package views.screen.register;

import controller.LoginController;
import controller.RegisterController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.User;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.login.LoginScreenHandler;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RegisterScreenHandler extends BaseScreenHandler implements Initializable {

    @FXML
    public Button btnBack;
    @FXML
    public Label lblRegisterErr;
    @FXML
    public TextField txfUsername;
    @FXML
    public Label lblUsernameErr;
    @FXML
    public PasswordField pwfPassword;
    @FXML
    public Label lblPasswordErr;
    @FXML
    public PasswordField pwfConfirmPassword;
    @FXML
    public Label lblConfirmPasswordErr;
    @FXML
    public Button btnRegister;

    public RegisterScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
    }

    /**
     * Redirect to Login screen
     *
     * @throws IOException
     */
    public void backToLogin() throws IOException {
        LoginScreenHandler loginHandler = new LoginScreenHandler(stage, Configs.LOGIN_SCREEN_PATH);
        loginHandler.setScreenTitle("Đăng nhập");
        loginHandler.show();
    }

    /**
     * Process the registration in UI, then validate the register info.
     * If all info is valid, redirect to Login screen
     */
    public void processRegister() {
        String username = txfUsername.getText();
        String password = pwfPassword.getText();
        String confirmPassword = pwfConfirmPassword.getText();

        lblUsernameErr.setVisible(username.isEmpty());
        lblPasswordErr.setVisible(password.isEmpty());

        if (!username.isEmpty() && !password.isEmpty()) {
            if (!confirmPassword.equals(password)) {
                lblConfirmPasswordErr.setText("Mật khẩu nhập lại không khớp");
                lblConfirmPasswordErr.setVisible(true);
            } else {
                User newUser = new User(username, password);
                if (getBController().addUser(newUser)) {
                    try {
                        LoginScreenHandler loginHandler = new LoginScreenHandler(stage, Configs.LOGIN_SCREEN_PATH);
                        loginHandler.setScreenTitle("Đăng nhập");
                        loginHandler.setBController(new LoginController());
                        loginHandler.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    lblRegisterErr.setVisible(true);
                }
            }
        }
    }

    public RegisterController getBController() {
        return (RegisterController) super.getBController();
    }

    /**
     * Set listeners for javafx components
     */
    public void setListeners() {
        btnRegister.setOnMouseClicked(mouseEvent -> processRegister());

        btnBack.setOnMouseClicked(mouseEvent -> {
            try {
                backToLogin();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setBController(new RegisterController());
        setListeners();
    }

    @Override
    public void show() {
        super.show();
    }
}
