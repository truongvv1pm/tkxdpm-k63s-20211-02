package views.screen.add_bike;

import java.io.IOException;
import java.net.URL;

import controller.AddBikeController;
import controller.ManageBikeController;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Bike;
import utils.exception.InvalidBikeInfoException;
import views.screen.BaseScreenHandler;
import javafx.scene.control.Button;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;


public class AddBikeScreenHandler extends BaseScreenHandler implements Initializable{
	@FXML
	private TextField type;
	@FXML
	private TextField brand;
	@FXML
	private TextField parkId;
	@FXML
	private Button submitButton, backBtn;
	@FXML
	private Label Error;
	@FXML
	private Label Success;
 
	boolean result;

	public AddBikeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	public void processInfo() throws SQLException, InterruptedException, IOException{
		Error.setVisible(false);
		Success.setVisible(false);
		String Type = type.getText().trim();	
	    String Brand = brand.getText().trim();
        String ParkId = parkId.getText().trim();
        result = getBController().validateInfo(Type, Brand, ParkId);
        if(!result) Error.setVisible(true);
        else Success.setVisible(true);   
	}
	
	void submitInfo() {
		submitButton.setOnMouseClicked(mouseEvent -> {
		try {
				processInfo();
            } catch (SQLException | InterruptedException | IOException e) {
                e.printStackTrace();
            }
		});
		
		backBtn.setOnMouseClicked(e -> this.getPreviousScreen().show());
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setBController(new ManageBikeController());
        submitInfo();
	}
	
	public AddBikeController getBController(){
        return (AddBikeController) super.getBController();
    }
	
	public void notifyError(){
		// TODO: implement later on if we need
	}
}
