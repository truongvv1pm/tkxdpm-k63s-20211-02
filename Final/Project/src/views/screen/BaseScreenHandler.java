package views.screen;

import controller.BaseController;
import javafx.scene.Scene;
import javafx.stage.Stage;
import views.screen.home.HomeScreenHandler;

import java.io.IOException;

public class BaseScreenHandler extends FXMLScreenHandler{

    protected final Stage stage;
    private Scene scene;
    protected HomeScreenHandler homeScreenHandler;
    private BaseScreenHandler prev;
    private BaseController bController;

    public BaseScreenHandler(Stage stage, String screenPath) throws IOException {
        super(screenPath);
        this.stage = stage;
        initialize();
    }

    public BaseScreenHandler(String screenPath) throws IOException {
        super(screenPath);
        this.stage = new Stage();
        initialize();
    }

    public BaseScreenHandler getPreviousScreen() {
        return this.prev;
    }

    public void setPreviousScreen(BaseScreenHandler prev) {
        this.prev = prev;
    }

    public void show() {
        if (this.scene == null) {
            this.scene = new Scene(this.content);
        }

        this.stage.setScene(this.scene);
        this.stage.show();
    }

    public void setScreenTitle(String string) {
        this.stage.setTitle(string);
    }

    public BaseController getBController() {
        return this.bController;
    }

    public void setHomeScreenHandler(HomeScreenHandler homeScreenHandler) {
        this.homeScreenHandler = homeScreenHandler;
    }

    public BaseScreenHandler getHomeScreenHandler() {
        return homeScreenHandler;
    }

    public void setBController(BaseController bController) {
        this.bController = bController;
    }

    public void initialize(){}
}
