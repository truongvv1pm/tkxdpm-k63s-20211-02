package subsystem;

import model.payment.PaymentCard;
import model.payment.PaymentTransaction;
import utils.exception.PaymentException;

/**
 * The {@code InterbankInterface} class is used to communicate with the
 * {@link InterbankSubsystem InterbankSubsystem} to make transaction
 *
 * @author hieud
 */
public interface InterbankInterface{

    /**
     * Pay order, and then return the payment transaction
     *
     * @param card     - the credit card used for payment
     * @param amount   - the amount to pay
     * @param contents - the transaction contents
     * @return {@link PaymentTransaction PaymentTransaction} - if the
     * payment is successful
     * @throws PaymentException if responded with a pre-defined error code
     */
    PaymentTransaction payOrder(PaymentCard card, int amount, String contents) throws PaymentException;

    /**
     * Refund, and then return the payment transaction
     *
     * @param card     - the credit card which would be refunded to
     * @param amount   - the amount to refund
     * @param contents - the transaction contents
     * @return {@link PaymentTransaction PaymentTransaction} - if the
     * payment is successful
     * @throws PaymentException if responded with a pre-defined error code
     */
    PaymentTransaction refund(PaymentCard card, int amount, String contents) throws PaymentException;

}
