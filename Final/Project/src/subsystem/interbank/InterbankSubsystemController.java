package subsystem.interbank;

import model.payment.CreditCard;
import model.payment.PaymentCard;
import model.payment.PaymentTransaction;
import utils.Configs;
import utils.Console;
import utils.MyMap;
import utils.Utils;
import utils.exception.PaymentException;


import java.util.Map;

public class InterbankSubsystemController implements InterbankSubsystemInterface{

    private static final String PAY_COMMAND = "pay";
    private static final String VERSION = "1.0.0";

    private static InterbankBoundary interbankBoundary = new InterbankBoundary();

    public PaymentTransaction refund(PaymentCard card, int amount, String contents){
        return null;
    }

    public boolean reset(PaymentCard card){
        Map<String, Object> transaction = new MyMap();

        try{
            transaction.putAll(MyMap.toMyMap(card));
        }catch(IllegalArgumentException|IllegalAccessException e){
            // TODO Auto-generated catch block
            return false;
        }

        interbankBoundary.query(Configs.RESET_BALANCE_URL, generateData(transaction));
        return true;
    }

    private String generateData(Map<String, Object> data){
        return ((MyMap) data).toJSON();
    }

    public PaymentTransaction payOrder(PaymentCard card, int amount, String contents) throws PaymentException{
        Map<String, Object> transaction = new MyMap();

        try{
            transaction.putAll(MyMap.toMyMap(card));
        }catch(IllegalArgumentException|IllegalAccessException e){
            // TODO Auto-generated catch block
            throw new PaymentException();
        }
        transaction.put("command", PAY_COMMAND);
        transaction.put("transactionContent", contents);
        transaction.put("amount", amount);
        transaction.put("createdAt", Utils.getToday());

        Map<String, Object> requestMap = new MyMap();
        requestMap.put("version", VERSION);
        requestMap.put("transaction", transaction);

        String responseText = interbankBoundary.query(Configs.PROCESS_TRANSACTION_URL, generateData(requestMap));
        MyMap response;
        try{
            response = MyMap.toMyMap(responseText, 0);
        }catch(IllegalArgumentException e){
            e.printStackTrace();
            throw new PaymentException();
        }

        return makePaymentTransaction(response);
    }

    private PaymentTransaction makePaymentTransaction(MyMap response) throws NullPointerException{
        if(response == null){
            return null;
        }
        MyMap transcation = (MyMap) response.get("transaction");
        String errorCode = (String) response.get("errorCode");
        Console.console("Error Code", errorCode);

        if(errorCode.equals("00")){
            PaymentCard card = new CreditCard((String) transcation.get("cardCode"), (String) transcation.get("owner"),
                    Integer.parseInt((String) transcation.get("cvvCode")), (String) transcation.get("dateExpired"));
            PaymentTransaction trans = new PaymentTransaction((String) response.get("errorCode"), card,
                    (String) transcation.get("transactionId"), (String) transcation.get("transactionContent"),
                    Integer.parseInt((String) transcation.get("amount")), (String) transcation.get("createdAt"));
            return trans;//success, other just null
        }else if ( errorCode.equals("01") ) {
            throw new PaymentException("Thông tin thẻ không chính xác!");
        } else if ( errorCode.equals("02") ) {
            throw new PaymentException("Số dư không đủ!");
        } else {
            throw new PaymentException("Có lỗi xảy ra!");
        }
    }

}
