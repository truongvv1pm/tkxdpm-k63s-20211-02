package subsystem;

import model.payment.PaymentCard;
import model.payment.PaymentTransaction;
import utils.exception.PaymentException;

public class VietinBank implements InterbankInterface{
    @Override
    public PaymentTransaction payOrder(PaymentCard card, int amount, String contents)
            throws PaymentException{
        /*another bank*/
        return null;
    }

    @Override
    public PaymentTransaction refund(PaymentCard card, int amount, String contents)
            throws PaymentException{
        return null;
    }
}
