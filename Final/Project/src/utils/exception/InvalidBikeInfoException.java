package utils.exception;

public class InvalidBikeInfoException extends EcoBikeException {
	public InvalidBikeInfoException(String message) {
		super(message);
	}
}
