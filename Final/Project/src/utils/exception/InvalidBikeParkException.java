package utils.exception;

public class InvalidBikeParkException extends EcoBikeException {

    public InvalidBikeParkException() {

    }

    public InvalidBikeParkException(String message) {
        super(message);
    }
}
