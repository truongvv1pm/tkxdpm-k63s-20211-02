package utils.exception;

public class UserNotRentingException extends EcoBikeException {

    public UserNotRentingException() {

    }

    public UserNotRentingException(String message) {
        super(message);
    }
}
