package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginForAdminController extends BaseController {

	
	public LoginForAdminController() {
		super();
	}
	
	/**
     * Check if admin's account with (username, password) exists in db
     */
    public boolean checkLogin(String username, String password) throws SQLException {
        String query = "SELECT * FROM admin WHERE username = ? AND password = ?";
        PreparedStatement pstm = dbConnection.prepareStatement(query);
        pstm.setString(1, username);
        pstm.setString(2, password);
        ResultSet results = pstm.executeQuery();
        return results.next();
    }
}
