package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginController extends BaseController {

    public LoginController() {
        super();
    }

    /**
     * Check if user with (username, password) exists in db
     */
    public boolean checkLogin(String username, String password) throws SQLException {
        String query = "SELECT * FROM user WHERE username = ? AND password = ?";
        PreparedStatement pstm = dbConnection.prepareStatement(query);
        pstm.setString(1, username);
        pstm.setString(2, password);
        ResultSet results = pstm.executeQuery();
        return results.next();
    }
}
