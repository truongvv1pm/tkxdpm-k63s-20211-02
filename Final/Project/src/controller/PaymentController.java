package controller;

import model.payment.CreditCard;
import model.payment.PaymentCard;
import subsystem.InterbankInterface;
import subsystem.InterbankSubsystem;
import utils.exception.PaymentException;

import java.util.Calendar;


/**
 * This {@code PaymentController} class control the flow of the payment process
 * in our AIMS Software.
 *
 * @author hieud
 */
public class PaymentController extends BaseController{

    /**
     * Represent the card used for payment
     */
    private PaymentCard card;

    /**
     * Represent the Interbank subsystem
     */
    private InterbankInterface interbank;

    /**
     * Validate the input date which should be in the format "mm/yy", and then
     * return a {@link String String} representing the date in the
     * required format "mmyy" .
     *
     * @param date - the {@link String String} represents the input date
     * @return {@link String String} - date representation of the required
     * format
     * @throws PaymentException - if the string does not represent a valid date
     *                          in the expected format
     */
    private String getExpirationDate(String date) throws PaymentException{
        String[] strs = date.split("/");
        if(strs.length != 2){
            throw new PaymentException();
        }

        String expirationDate;
        int month = -1;
        int year = -1;

        try{
            month = Integer.parseInt(strs[0]);
            year = Integer.parseInt(strs[1]);
            if(month < 1 || month > 12 || year < Calendar.getInstance().get(Calendar.YEAR) % 100 || year > 100){
                throw new PaymentException();
            }
            expirationDate = strs[0] + strs[1];

        }catch(Exception ex){
            throw new PaymentException();
        }

        return expirationDate;
    }

    /**
     * Pay order, and then return the result with a message.
     *
     * @param amount         - the amount to pay
     * @param contents       - the transaction contents
     * @param cardNumber     - the card number
     * @param cardHolderName - the card holder name
     * @param expirationDate - the expiration date in the format "mm/yy"
     * @param securityCode   - the cvv/cvc code of the credit card
     * @return true if success, else false
     */
    public boolean payOrder(int amount, String contents, String cardNumber, String cardHolderName,
                            String expirationDate, String securityCode){
        this.card = new CreditCard(cardNumber, cardHolderName, Integer.parseInt(securityCode),
                getExpirationDate(expirationDate));

        this.interbank = new InterbankSubsystem();
        interbank.payOrder(card, amount, contents);

        return true;
    }

    /**
     * @param cardNumber
     * @param cardHolderName
     * @param expirationDate
     * @param securityCode
     */
    public boolean refund(String cardNumber, String cardHolderName, String expirationDate, String securityCode,
                          int amount, String contents){
        this.interbank = new InterbankSubsystem();
        this.card = new CreditCard(cardNumber, cardHolderName, Integer.parseInt(securityCode),
                getExpirationDate(expirationDate));
        interbank.refund(card, amount, contents);
        return true;
    }

    /**
     * @param cardNumber
     * @param cardHolderName
     * @param expirationDate
     * @param securityCode
     */
    public boolean reset(String cardNumber, String cardHolderName, String expirationDate, String securityCode){
        this.interbank = new InterbankSubsystem();
        this.card = new CreditCard(cardNumber, cardHolderName, Integer.parseInt(securityCode),
                getExpirationDate(expirationDate));
        return ((InterbankSubsystem) interbank).reset(card);
    }
}