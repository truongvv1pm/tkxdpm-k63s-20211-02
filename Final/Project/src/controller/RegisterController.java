package controller;

import model.User;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegisterController extends BaseController {

    public RegisterController() {
        super();
    }

    public boolean addUser(User user) {
        try {
            String query = "INSERT INTO user(username, password) VALUES(?, ?)";
            PreparedStatement pstm = dbConnection.prepareStatement(query);
            pstm.setString(1, user.getUsername());
            pstm.setString(2, user.getPassword());
            pstm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
