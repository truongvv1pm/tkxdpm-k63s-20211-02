package controller;

import model.Bike;
import model.Park;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ParkController extends BaseController implements ParkControllerInterface {

    public ParkController() {
        super();
    }

    /**
     * @return
     */
    public List<Park> getAllPark() throws SQLException {
        List<Park> parkList = new ArrayList<>();
        String query = "SELECT * FROM park;";
        PreparedStatement pstm = dbConnection.prepareStatement(query);
        ResultSet results = pstm.executeQuery();
        while (results.next()) {
            Park park = new Park();

            park.setId(results.getInt(1));
            park.setName(results.getString(2));
            park.setAddress(results.getString(3));
            park.setMaxSlot(results.getInt(4));
            park.setCurrentSlot(getCurrentParkSlot(park.getId()));

            parkList.add(park);
        }

        return parkList;
    }

    /**
     * get current park slot, not include current rented bike
     *
     * @param parkId
     * @return
     * @throws SQLException
     */
    public int getCurrentParkSlot(int parkId) throws SQLException {
        String query = "SELECT COUNT(*) FROM bike LEFT JOIN rental on bike.id = rental.bike_id WHERE bike.park_id = ? and rental.id IS NULL;";
        PreparedStatement pstm = dbConnection.prepareStatement(query);
        pstm.setInt(1, parkId);
        ResultSet results = pstm.executeQuery();
        results.next();
        return results.getInt(1);
    }

    /**
     * get max park slot
     *
     * @param parkId
     * @return
     * @throws SQLException
     */
    public int getMaxParkSlot(int parkId) throws SQLException {
        String query = "SELECT max_slot FROM park WHERE id = ?";
        PreparedStatement pstm = dbConnection.prepareStatement(query);
        pstm.setInt(1, parkId);
        ResultSet results = pstm.executeQuery();
        results.next();
        return results.getInt(1);
    }

    /**
     * get all bike in park
     *
     * @param parkId
     * @return
     * @throws SQLException
     */
    public List<Bike> getAllBikeInPark(int parkId){
        List bikeList = new ArrayList<>();

        try{
            PreparedStatement statement = dbConnection.prepareStatement("SELECT * FROM bike WHERE park_id = ?;");
            statement.setInt(1, parkId);
            ResultSet rs = statement.executeQuery();

            while(rs.next()){
                Bike bike = new Bike();
                bike.setId(rs.getInt(1));
                bike.setType(rs.getString(2));
                bike.setBrand(rs.getString(3));
                bike.setParkId(rs.getInt(4));
                bike.setStatus(new BikeController().getBikeStatus(bike.getId()));

                bikeList.add(bike);
            }

        }catch(SQLException throwables){
            throwables.printStackTrace();
        }

        return bikeList;
    }

    public boolean validateParkInfo(String name, String address) {
    	if(name == null || address == null) return false;
    	
		if (!Pattern.matches("[a-zA-Z\\s]*{1,100}", name) || (!Pattern.matches("[a-zA-Z\\s.]*{1,200}", address))) return false;
		
    	return true;
    } 

    public boolean validateParkName(String name) {
        if (name == null) return false;

        return Pattern.matches("[a-zA-Z\\s]*{1,100}", name);
    }

    public boolean validateParkAddress(String address)  throws SQLException{
        if(address == null) return false;

        if(checkParkExisted(address)) return false;

        return Pattern.matches("[a-zA-Z\\s.]*{1,200}", address);
    }
    
     /**
     * check if new park's address has existed in database
     * @param address
     * @return
     * @throws SQLException
     */
    public boolean checkParkExisted(String address) throws SQLException {
    	String query = "SELECT count(id) AS total FROM park WHERE address = ?";
        PreparedStatement pstm = dbConnection.prepareStatement(query);
        pstm.setString(1, address);
        ResultSet result = pstm.executeQuery();
        
        if (result.getInt("total") > 0) return true;
    	return false;
    }
    
    /**
     * add new park to database
     * 
     * @param name 
     * @param address
     * @param capacity - maxSlot
     * @throws SQLException
     */
    public boolean addPark(String name, String address, int capacity) throws SQLException{
    	if(!checkParkExisted(address)) {
    		try {
                String query = "INSERT INTO park(name, address, max_slot) VALUES (?, ?, ?);";
                PreparedStatement pstm = dbConnection.prepareStatement(query);
                pstm.setString(1, name);
                pstm.setString(2, address);
                pstm.setInt(3, capacity);
                pstm.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
            return true;
    	}
        return false;
    }
}
