package controller;

import database.connection.SQLiteConnection;

import java.sql.Connection;

public class BaseController {

    /**
     * Connection object for by all Controllers that extends BaseController
     */
    protected Connection dbConnection;

    public BaseController() {
        dbConnection = SQLiteConnection.getConnection();
    }

}
