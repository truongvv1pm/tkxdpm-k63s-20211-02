package controller;

import model.Bike;

import java.sql.SQLException;

public interface BikeControllerInterface{
    String getBikeStatus(int bikeId);
    Bike getBikeById(int bikeId);

    /**
     * Park the bike with bikeId at the park with parkId
     * @param bikeId
     * @param parkId
     */
    void updateBikePark(int bikeId, int parkId) throws SQLException;
}
