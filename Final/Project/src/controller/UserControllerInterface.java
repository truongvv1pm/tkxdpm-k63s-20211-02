package controller;

import model.Rental;

public interface UserControllerInterface{

    /**
     *
     * @param username
     * @param password
     * @return
     */
    boolean validatePassword(String username, String password);

    /**
     *
     * @param username
     * @return current user rental. If user is not renting, return null.
     */
    Rental getUserRental(String username);
}
