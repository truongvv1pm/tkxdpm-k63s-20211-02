package controller;

import model.Bike;
import model.Rental;
import utils.exception.InvalidRentalException;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class RentalController extends BaseController implements RentalControllerInterface{

    private final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    private final BikeControllerInterface bikeController = new BikeController();

    @Override
    /**
     * See {@link RentalControllerInterface#calculateRentFee(Rental)}
     */ public long calculateRentFee(Rental rental){
        Bike bike = bikeController.getBikeById(rental.getBikeId());
        long rentTime = calculateRentTime(rental.getStartTime()) / 60000;
        long baseFee = 0;
        if(rentTime <= 10){
            baseFee = 0;
        }else if(rentTime <= 30){
            baseFee = 10000;
        }else{
            baseFee = 10000 + ((rentTime - 31) / 15 + 1) * 3000;
        }
        if(bike.getType().equals("single")){
            return baseFee;
        }
        if(bike.getType().equals("twin")|bike.getType().equals("single-electric")){
            return (long) (baseFee * 1.5);
        }
        throw new InvalidRentalException("No such bike type");
    }

    @Override
    /**
     * See {@link RentalControllerInterface#calculateRentFee(Rental)}
     */ public long calculateRentTime(String startTime){
        long diff = -1;
        try{
            Date startRentTime = DATE_FORMATTER.parse(startTime);
            Date now = new Date();
            diff = now.getTime() - startRentTime.getTime();
        }catch(ParseException e){
            e.printStackTrace();
        }
        return diff;
    }

    @Override
    /**
     * See {@link RentalControllerInterface#deleteRental(int)}
     */ public void deleteRental(int rentalId) throws SQLException{
        PreparedStatement statement = dbConnection.prepareStatement("DELETE FROM rental WHERE id=?");
        statement.setInt(1, rentalId);
        statement.execute();
    }

    @Override
    public Integer calculateReserveFee(Bike bike, Integer hour, Integer min) throws InvalidRentalException{
        return new EcoBikeReserveCalculator().calculateReservingFee(bike, hour, min);
    }

    @Override
    public int rentBike(String username, int duration, Date startTime, int bikeId, int fee){
        try{
            Rental rental = new Rental(username, bikeId, DATE_FORMATTER.format(startTime), duration, fee);

            PreparedStatement statement = dbConnection.prepareStatement(
                    "INSERT INTO rental(username, bike_id, start_time, duration, fee) VALUES (?,?,?,?,?)");
            statement.setString(1, rental.getUsername());
            statement.setInt(2, rental.getBikeId());
            statement.setString(3, rental.getStartTime());
            statement.setInt(4, rental.getDuration());
            statement.setInt(5, rental.getFee());
            return statement.executeUpdate();
        }catch(SQLException throwables){
            throwables.printStackTrace();
        }
        return 0;
    }

    /**
     * get all bike in park
     *
     * @param parkId
     * @return
     * @throws SQLException
     */
    public List<Bike> getAllBikeInPark(int parkId){
        return new ParkController().getAllBikeInPark(parkId);
    }

    /**
     * @param bikeId
     * @return 'available' | 'rented'
     */
    public String getBikeStatus(int bikeId){
        return new BikeController().getBikeStatus(bikeId);
    }

    /**
     * return true if user is renting
     *
     * @param username
     * @return
     * @throws SQLException
     */
    public boolean checkUserRental(String username){
        return new UserController().getUserRental(username) != null;
    }
}
