package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class controls the flow of Add bike use-case in our project
 * @author YenNH
 *
 */
public class AddBikeController extends BaseController{
	
	private ParkControllerInterface parkController;
	
	/**
	 * validate the new bike information  
	 */
	public boolean validateInfo(String type, String brand, String parkId) throws InterruptedException, IOException, SQLException{
		if(validateId(parkId) && validateType(type) && validateBrand(brand)) {
			int newParkId = Integer.parseInt(parkId);
			addBike(type, brand, newParkId);
			return true;
		}
		else return false;
	}
	
	public boolean validateId(String id) throws SQLException {
		if(id == null) return false;
		try {
			Integer.parseInt(id);
    	} catch(NumberFormatException e) {
    		return false;
    	}
		int parkId = Integer.parseInt(id);
	    return validatePark(parkId);
	}
	
	public boolean validatePark(int parkId) throws SQLException {
		parkController = new ParkController();
		String query = "SELECT * FROM park WHERE id = ?";
        PreparedStatement ps = dbConnection.prepareStatement(query);
        ps.setInt(1, parkId);
        ResultSet results = ps.executeQuery();
        if (!results.next()) return false;
        int currentSlot = parkController.getCurrentParkSlot(parkId);
        int maxSlot = parkController.getMaxParkSlot(parkId);
        if (currentSlot >= maxSlot) return false;
        else return true;            		
    }
	
	public boolean validateType(String type) {
		if(type == null) return false;
		if((!type.equals("twin")) && (!type.equals("single"))) return false;
		return type.matches("([A-Za-z\\\\s])*{1,100}" );
	}

	public boolean validateBrand(String brand) {
		if(brand == null) return false;
		return brand.matches("([A-Za-z\\\\s])*{1,100}" );
	}
	
	
	/**
	 * add new bike in database
	 * @throws SQLException
	 */
	public void addBike(String type, String brand, int parkId) throws SQLException {
		try {
            String query = "INSERT INTO bike(type, brand, park_id) VALUES(?, ?, ?)";
            PreparedStatement ps = dbConnection.prepareStatement(query);
            ps.setString(1, type);
            ps.setString(2, brand);
            ps.setInt(3, parkId);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
 
	}
}
