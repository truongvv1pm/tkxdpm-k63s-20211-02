package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Bike;

public class ManageBikeController extends BaseController{

	public ManageBikeController() {
		super();
	}
	
	public List<Bike> getAllBike(){
        List bikeList = new ArrayList<>();

        try{
            PreparedStatement statement = dbConnection.prepareStatement("SELECT * FROM bike ");
            ResultSet rs = statement.executeQuery();

            while(rs.next()){
                Bike bike = new Bike();
                bike.setId(rs.getInt(1));
                bike.setType(rs.getString(2));
                bike.setBrand(rs.getString(3));
                bike.setParkId(rs.getInt(4));
                bike.setStatus(getBikeStatus(bike.getId()));

                bikeList.add(bike);
            }

        }catch(SQLException throwables){
            throwables.printStackTrace();
        }

        return bikeList;
    }
	
	public String getBikeStatus(int bikeId){
        String rs = "rented";
        try{
            PreparedStatement statement = dbConnection.prepareStatement("SELECT * FROM rental WHERE bike_id = ?;");
            statement.setInt(1, bikeId);
            ResultSet resultSet = statement.executeQuery();
            if(!resultSet.next()){
                rs = "available";
            }


        }catch(SQLException throwables){
            throwables.printStackTrace();
        }

        return rs;
    }
}
