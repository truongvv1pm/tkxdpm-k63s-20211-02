package database.connection;

import java.sql.Connection;

public interface DBConnection{
    Connection initConnection();
}
