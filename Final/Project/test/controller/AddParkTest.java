package controller;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ParkController;

class AddParkTest {
	
	private ParkController parkController;

	@BeforeEach
	void setUp() throws Exception {
		parkController = new ParkController();
	}

	@ParameterizedTest
	@CsvSource({
		"EcoBike HN, Hanoi, true",
		"@EcoBike, Hai Phong, false",
		"EcoBike, @vn, false"
	})

	@Test
	void test(String name, String address, boolean expected) throws SQLException{
		boolean isValid = parkController.validateParkInfo(name, address);
		
		assertEquals(expected, isValid);
	}

}
