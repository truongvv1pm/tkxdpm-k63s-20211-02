package controller;

import model.Bike;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import utils.Console;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RentalControllerTest{

    RentalController rentalController;
    DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

    @BeforeEach
    void setUp(){
        rentalController = new RentalController();
    }

    @ParameterizedTest
    @CsvSource({"single, 10, 0", "single, 20, 10000", "single, 30, 10000", "single, 40, 13000", "single, 50, 16000",
            "single, 90, 22000"})
    void calculateRentFeeTest(String bikeType, long rentTime, long expected){
        long baseFee = 0;
        if(rentTime <= 10){
            baseFee = 0;
        }else if(rentTime <= 30){
            baseFee = 10000;
        }else{
            baseFee = 10000 + ((rentTime - 31) / 15 + 1) * 3000;
        }
        assertEquals(expected, baseFee);
    }

    @ParameterizedTest
    @CsvSource({"2020-01-01T00:01, 2020-01-01T00:11, 600000", "2020-01-01T00:01, 2020-01-01T00:12, 660000",
            "2020-01-01T00:01, 2020-01-01T01:01, 3600000", "2020-01-01T00:01, 2020-01-02T00:01, 86400000"})
    void calculateRentTimeTest(String startTimeString, String nowString, long expected) throws ParseException{
        Date startTime = DATE_FORMATTER.parse(startTimeString);
        Date now = DATE_FORMATTER.parse(nowString);
        long diff = now.getTime() - startTime.getTime();
        assertEquals(expected, diff);
    }

    @ParameterizedTest
    @CsvSource({"1,0,10", "1,1,20", "2,0,0", "6,0,10", "29,1,10", "1000,1,10"})
    void calculateReserveFeeTest(int bikeId, int hour, int min){
        Bike bike = new BikeController().getBikeById(bikeId);
        try{
            int fee = rentalController.calculateReserveFee(bike, hour, min);
        }catch(Exception ignored){
            Console.console("Expected to catch exception if bike is null");
        }
        /*expected no exception if bike is null*/
    }

    @ParameterizedTest
    @CsvSource({"1,0,10", "1,1,20", "2,0,0", "6,0,10", "29,1,10", "1000,1,10"})
    void rentBikeTest(int bikeId, int duration, int fee, String username, String startTime){
        Bike bike = new BikeController().getBikeById(bikeId);
        try{
            int rs = rentalController.rentBike(username, duration, DATE_FORMATTER.parse(startTime), bikeId, fee);
            assertEquals(rs,1);
        }catch(Exception ignored){
        }
        /*expected no exception if bike is null*/
    }
}