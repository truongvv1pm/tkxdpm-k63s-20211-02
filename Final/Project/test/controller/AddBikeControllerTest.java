package controller;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.AddBikeController;

class AddBikeControllerTest {

	private AddBikeController addBikeController;
	@BeforeEach
	void setUp() throws Exception {
		addBikeController = new AddBikeController();
	}

	@ParameterizedTest
    @CsvSource({
    	"single, TN, 2, true",
    	", HN, , false",
    	"HN, TN, 2, false"
    }
    )
	void test(String type, String brand, String parkId, boolean expected) throws InterruptedException, IOException, SQLException {
		boolean isValid = addBikeController.validateInfo(type, brand, parkId);
		assertEquals(expected, isValid);
	}
}
