package views.screen.manage_bike;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import controller.AddBikeController;
import controller.ManageBikeController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Bike;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.add_bike.AddBikeScreenHandler;

public class ManageBikeScreenHandler extends BaseScreenHandler implements Initializable {
	@FXML
    private HBox hBox;
    @FXML
    private Button addBikeButton;
    @FXML
    private Button backBtn;
    private BaseScreenHandler parent;
    
	public ManageBikeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	public void setListeners() {
		addBikeButton.setOnMouseClicked(mouseEvent -> {
			try {
                // Redirect to ManageBike page
                AddBikeScreenHandler addBikeHandler = new AddBikeScreenHandler(stage, Configs.ADD_BIKE_SCREEN_PATH);
                addBikeHandler.setScreenTitle("Thêm Xe");
                addBikeHandler.setBController(new AddBikeController());
                addBikeHandler.setPreviousScreen(this);
                addBikeHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
		});
		backBtn.setOnMouseClicked(e -> this.getPreviousScreen().show());
	}
		
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setBController(new ManageBikeController());
		loadBikes();
        setListeners();
	}
	
	
	public void loadBikes(){
        this.setBController(new ManageBikeController());

        List bikeHandlerList = new ArrayList<>();
        /*load all bike*/
        List bikeList = this.getBController().getAllBike();

        bikeList.forEach(bike -> {
            try{
                BikeItemHandler bikeItemHandler = new BikeItemHandler(stage,
                        Configs.BIKE_DETAIL);
                bikeItemHandler.setBController(new ManageBikeController());
                bikeItemHandler.setBike((Bike) bike);
                bikeItemHandler.setParent(this);

                bikeHandlerList.add(bikeItemHandler);

            }catch(IOException e){
                e.printStackTrace();
            }
        });

        layoutBikes(bikeHandlerList);
    }
	
	public ManageBikeController getBController(){
        return (ManageBikeController) super.getBController();
    }
	
	private void layoutBikes(List bikeHandlers){
        ArrayList bikeHandlerArrayList = (ArrayList) ((ArrayList) bikeHandlers).clone();

        hBox.getChildren().clear();

        while(!bikeHandlerArrayList.isEmpty()){
            VBox vBox;
            hBox.getChildren().add(vBox = new VBox());
            vBox.setStyle("-fx-border-color: blue");

            while((vBox.getChildren().size() < 2) && !bikeHandlerArrayList.isEmpty()){
                BikeItemHandler bikeHandler = (BikeItemHandler) bikeHandlerArrayList.get(0);
                vBox.getChildren().add(bikeHandler.getContent());
                bikeHandlerArrayList.remove(bikeHandler);
            }

        }
    }
	
	public void setParentScene(BaseScreenHandler handler){
        this.parent = handler;
    }

}
