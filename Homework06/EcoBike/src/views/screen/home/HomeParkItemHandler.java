package views.screen.home;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Park;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.park.ParkRentalDetailHandler;

import java.io.IOException;

public class HomeParkItemHandler extends BaseScreenHandler{
    private Park park;
    private BaseScreenHandler parent;

    @FXML
    private Label nameLabel, addressLabel, maxSlotLabel, availableSlotLabel;
    @FXML
    private Button viewBikeBtn;

    public HomeParkItemHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);

        viewBikeBtn.setOnMouseClicked(e -> {
            try{
                viewParkDetail();
            }catch(IOException ex){
                ex.printStackTrace();
            }
        });
    }

    public void setPark(Park park){
        this.park = park;
        this.nameLabel.setText(park.getName());
        this.addressLabel.setText(park.getAddress());
        this.maxSlotLabel.setText(String.valueOf(park.getMaxSlot()));
        this.availableSlotLabel.setText(String.valueOf(park.getMaxSlot() - park.getCurrentSlot()));
    }

    private void viewParkDetail() throws IOException{
        // TODO: go to park detail screen (bikes layouts)
        ParkRentalDetailHandler parkRentalDetailHandler = new ParkRentalDetailHandler(stage,
                Configs.PARK_RENTAL_DETAIL_SCREEN);
        parkRentalDetailHandler.setPark(park);
        parkRentalDetailHandler.setScreenTitle("Chi Tiết Bãi Xe");
        parkRentalDetailHandler.setPreviousScreen(this.parent);
        parkRentalDetailHandler.show();
    }

    public void setParentScene(BaseScreenHandler handler){
        this.parent = handler;
    }


}
