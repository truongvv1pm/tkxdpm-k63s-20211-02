package views.screen.admin_settings;

import controller.BikeController;
import controller.LoginController;
import controller.ParkController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.login.LoginScreenHandler;
import views.screen.manage_bike.ManageBikeScreenHandler;
import views.screen.manage_park.ManageParkScreenHandler;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AdminSettingsScreenHandler extends BaseScreenHandler implements Initializable{

	@FXML
	private Button btnManageBike, btnManagePark, btnManageUser, btnLogOut;
	
	public AdminSettingsScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}

	public void setListeners() {
		btnManageBike.setOnMouseClicked(mouseEvent -> {
			try {
                // Redirect to ManageBike page
                ManageBikeScreenHandler manageBikeHandler = new ManageBikeScreenHandler(stage, Configs.MANAGE_BIKE_SCREEN_PATH);
                manageBikeHandler.setScreenTitle("Quản lý Xe");
                manageBikeHandler.setBController(new BikeController());
                manageBikeHandler.setPreviousScreen(this);
                manageBikeHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
		});
		
		btnManagePark.setOnMouseClicked(mouseEvent -> {
			try {
                // Redirect to ManageBike page
                ManageParkScreenHandler manageParkHandler = new ManageParkScreenHandler(stage, Configs.MANAGE_PARK_SCREEN_PATH);
                manageParkHandler.setScreenTitle("Quản lý Bãi xe");
                manageParkHandler.setBController(new ParkController());
                manageParkHandler.setPreviousScreen(this);
                manageParkHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
		});
		
		btnLogOut.setOnMouseClicked(event -> {
            try {
                // Show Login screen
                LoginScreenHandler loginScreenHandler = new LoginScreenHandler(stage, Configs.LOGIN_SCREEN_PATH);
                loginScreenHandler.setScreenTitle("Đăng nhập");
                loginScreenHandler.setBController(new LoginController());
                loginScreenHandler.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setBController(new ParkController());
        setListeners();
	}

}
