package views.screen.manage_park;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Park;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.park.ParkRentalDetailHandler;

public class ManageParkItemHandler extends BaseScreenHandler {

	private Park park;
    private BaseScreenHandler parent;

    @FXML
    private Label nameLabel, addressLabel, maxSlotLabel, availableSlotLabel;
    @FXML
    private Button viewBikeBtn;

    public ManageParkItemHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);
    }

    public void setPark(Park park){
        this.park = park;
        this.nameLabel.setText(park.getName());
        this.addressLabel.setText(park.getAddress());
        this.maxSlotLabel.setText(String.valueOf(park.getMaxSlot()));
        this.availableSlotLabel.setText(String.valueOf(park.getMaxSlot() - park.getCurrentSlot()));
    }

    public void setParentScene(BaseScreenHandler handler){
        this.parent = handler;
    }
    
}
