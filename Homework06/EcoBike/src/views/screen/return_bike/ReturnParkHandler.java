package views.screen.return_bike;

import controller.ReturnBikeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Park;
import utils.Configs;
import utils.exception.InvalidBikeParkException;
import views.screen.BaseScreenHandler;
import views.screen.popup.NotificationPopupHandler;

import java.io.IOException;
import java.sql.SQLException;

public class ReturnParkHandler extends BaseScreenHandler {

    @FXML
    Label lblParkName;
    @FXML
    Label lblParkPosition;
    @FXML
    Label lblCurrentSlot;
    @FXML
    Label lblMaxSlot;
    @FXML
    Button btnSelectPark;

    private Park park;

    public ReturnParkHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
        btnSelectPark.setOnMouseClicked(event -> setBtnSelectPark());
    }

    /**
     * Get user selection
     */
    public void setBtnSelectPark() {
        try {
            getBController().validateBikeVacancy(park.getId());
        } catch (InvalidBikeParkException e) {
            try {
                NotificationPopupHandler.failure("Bãi xe không khả dụng");
                e.printStackTrace();
                return;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            ReturnBikeInvoiceHandler returnBikeInvoiceHandler = new ReturnBikeInvoiceHandler(stage, Configs.RETURN_BIKE_INVOICE_PATH);
            returnBikeInvoiceHandler.setScreenTitle("Thông tin thanh toán trả xe");
            returnBikeInvoiceHandler.setBController(getBController());
            returnBikeInvoiceHandler.setPark(park);
            returnBikeInvoiceHandler.setRental(getBController().getUserRental(Configs.username));
            returnBikeInvoiceHandler.setContents();
            returnBikeInvoiceHandler.setHomeScreenHandler(homeScreenHandler);
            returnBikeInvoiceHandler.setPreviousScreen(this);
            returnBikeInvoiceHandler.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set name, position, current slot and max slot for each object to be displayed
     */
    public void setContents() {
        lblParkName.setText(park.getName());
        lblParkPosition.setText(park.getAddress());
        lblCurrentSlot.setText(String.valueOf(park.getCurrentSlot()));
        lblMaxSlot.setText(String.valueOf(park.getMaxSlot()));
    }

    /**
     * Set the entity park for the displayed object
     *
     * @param park
     */
    public void setPark(Park park) {
        this.park = park;
    }

    public ReturnBikeController getBController() {
        return (ReturnBikeController) super.getBController();
    }
}
