package views.screen.return_bike;

import controller.PaymentController;
import controller.ReturnBikeController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Park;
import model.Rental;
import utils.Configs;
import utils.exception.EcoBikeException;
import views.screen.BaseScreenHandler;
import views.screen.payment.PaymentFormHandler;
import views.screen.popup.NotificationPopupHandler;

import java.io.IOException;

public class ReturnBikeInvoiceHandler extends BaseScreenHandler {

    @FXML
    Label lblRentHour;
    @FXML
    Label lblRentMinute;
    @FXML
    Label lblRentMoney;
    @FXML
    Label lblRentDeposit;
    @FXML
    Label lblRentTotal;
    @FXML
    Button btnCancel;
    @FXML
    Button btnPayment;

    private Rental rental;
    private Park park;
    private long total;

    public ReturnBikeInvoiceHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
        btnCancel.setOnMouseClicked(event -> {
            getHomeScreenHandler().show();
        });
        btnPayment.setOnMouseClicked(event -> processPayment());
    }

    /**
     * When payment button is click, show PaymentForm
     */
    private void processPayment() {
        try {
            PaymentFormHandler paymentFormHandler = new PaymentFormHandler(stage, Configs.PAYMENT_FORM);
            paymentFormHandler.setCallback(booleanStringPair -> {
                if (booleanStringPair.getKey()) {
                    // If payment is successful, return bike is successful
                    try {
                        // Delete rental
                        deleteRental();

                        // Update bike park (change parkId)
                        updateBikePark();

                        // Update home
                        getHomeScreenHandler().initialize();

                        // Return home
                        getHomeScreenHandler().show();

                        NotificationPopupHandler.success("Trả Xe Thành Công");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        NotificationPopupHandler.failure(booleanStringPair.getValue());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            });
            paymentFormHandler.setBController(new PaymentController());
            paymentFormHandler.setContent("Thanh toán trả xe");
            paymentFormHandler.setFee((int) total);
            paymentFormHandler.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update bike park when return bike is successful
     */
    private void updateBikePark() {
        try {
            getBController().updateBikePark(rental.getBikeId(), park.getId());
        } catch (EcoBikeException e) {
            e.printStackTrace();
            try {
                NotificationPopupHandler.failure(e.getMessage());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Delete rental when return bike is successful
     */
    private void deleteRental() {
        try {
            getBController().deleteRental(rental.getId());
        } catch (EcoBikeException e) {
            e.printStackTrace();
            try {
                NotificationPopupHandler.failure(e.getMessage());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void setRental(Rental rental) {
        this.rental = rental;
    }

    public void setPark(Park park) {
        this.park = park;
    }

    public void setContents() {
        long rentTime = getBController().calculateRentTime(rental.getStartTime());
        long rentHour = rentTime / (60 * 60 * 1000);
        long rentMinute = rentTime / (60 * 1000) % 60;
        long rentMoney = getBController().calculateRentFee(rental);
        long rentFee = rental.getFee();
        long total = rentMoney - rentFee;
        lblRentHour.setText(String.valueOf(rentHour));
        lblRentMinute.setText(String.valueOf(rentMinute));
        lblRentMoney.setText(String.valueOf(rentMoney));
        lblRentDeposit.setText(String.valueOf(rentFee));
        lblRentTotal.setText(String.valueOf(total));
        this.total = total;
    }

    public ReturnBikeController getBController() {
        return (ReturnBikeController) super.getBController();
    }
}
