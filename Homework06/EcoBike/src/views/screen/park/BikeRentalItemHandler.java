package views.screen.park;

import controller.RentalController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import model.Bike;
import utils.Configs;
import utils.exception.InvalidRentalException;
import views.screen.BaseScreenHandler;
import views.screen.rent_bike.ConfirmRentingPopupHandler;
import views.screen.popup.NotificationPopupHandler;

import java.io.IOException;
import java.sql.SQLException;

public class BikeRentalItemHandler extends BaseScreenHandler{
    @FXML
    private Label typeLabel, brandLabel, statusLabel;
    @FXML
    private ComboBox hourBox, minuteBox;
    @FXML
    private Button rentBtn;

    private Bike bike;
    private BaseScreenHandler parent;

    public BikeRentalItemHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);
        rentBtn.setOnMouseClicked(e -> {
            try{
                onRentalPress();
            }catch(InvalidRentalException|SQLException|IOException exception){
                exception.printStackTrace();
                try{
                    NotificationPopupHandler.failure(exception.getMessage());
                }catch(IOException ex){
                    ex.printStackTrace();
                }
            }
        });
    }

    /**
     * set display bike
     *
     * @param bike
     */
    public void setBike(Bike bike){
        this.bike = bike;
        this.brandLabel.setText(bike.getBrand());
        this.typeLabel.setText(bike.getType());
        this.statusLabel.setText(bike.getStatus());

        if(bike.getStatus().equals("rented")){
            rentBtn.setDisable(true);
        }
    }

    public void setParent(BaseScreenHandler handler){
        this.parent = handler;
    }

    public RentalController getBController(){
        return (RentalController) super.getBController();
    }

    /**
     * @throws InvalidRentalException
     */
    private void onRentalPress() throws InvalidRentalException, SQLException, IOException{
        /*check if user is renting*/
        if(getBController().checkUserRental(Configs.username)){
            throw new InvalidRentalException("User is renting");
        }
        Integer hour = Integer.parseInt(String.valueOf(hourBox.getValue())), minute = Integer.parseInt(
                String.valueOf(minuteBox.getValue()));

        /*again, check bike availability again*/
        String currentStatus = getBController().getBikeStatus(bike.getId());
        if(currentStatus.equals("rented")){
            bike.setStatus("rented");
            setBike(bike);
            throw new InvalidRentalException("Bike is rented");
        }

        /*calculate reserve price*/
        int fee = getBController().calculateReserveFee(bike, hour, minute);
        ConfirmRentingPopupHandler.show(bike, hour, minute, fee, this.parent);
    }

}
