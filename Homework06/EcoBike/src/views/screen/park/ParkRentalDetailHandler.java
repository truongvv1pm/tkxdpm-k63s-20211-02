package views.screen.park;

import controller.RentalController;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Bike;
import model.Park;
import utils.Configs;
import views.screen.BaseScreenHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParkRentalDetailHandler extends BaseScreenHandler{
    @FXML
    private HBox hBox;
    @FXML
    private Button backBtn;
    @FXML
    private Label nameLabel, addressLabel;
    private Park park;

    public ParkRentalDetailHandler(Stage stage, String screenPath) throws IOException{
        super(stage, screenPath);
        backBtn.setOnMouseClicked(e -> this.getPreviousScreen().show());
    }

    /**
     * display bikes and park
     *
     * @param park
     */
    public void setPark(Park park){
        this.park = park;
        nameLabel.setText(park.getName());
        addressLabel.setText(park.getAddress());
        loadBikes();
    }

    /**
     *
     */
    public void loadBikes(){
        this.setBController(new RentalController());

        List bikeHandlerList = new ArrayList<>();
        /*load all bike*/
        List bikeList = this.getBController().getAllBikeInPark(park.getId());

        bikeList.forEach(bike -> {
            try{
                BikeRentalItemHandler bikeRentalItemHandler = new BikeRentalItemHandler(stage,
                        Configs.BIKE_RENTAL_DETAIL_ITEM);
                bikeRentalItemHandler.setBController(new RentalController());
                bikeRentalItemHandler.setBike((Bike) bike);
                bikeRentalItemHandler.setParent(this);

                bikeHandlerList.add(bikeRentalItemHandler);

            }catch(IOException e){
                e.printStackTrace();
            }
        });

        layoutBikes(bikeHandlerList);
    }


    public RentalController getBController(){
        return (RentalController) super.getBController();
    }

    private void layoutBikes(List bikeHandlers){
        ArrayList bikeHandlerArrayList = (ArrayList) ((ArrayList) bikeHandlers).clone();

        hBox.getChildren().clear();

        while(!bikeHandlerArrayList.isEmpty()){
            VBox vBox;
            hBox.getChildren().add(vBox = new VBox());
            vBox.setStyle("-fx-border-color: blue");

            while((vBox.getChildren().size() < 2) && !bikeHandlerArrayList.isEmpty()){
                BikeRentalItemHandler bikeHandler = (BikeRentalItemHandler) bikeHandlerArrayList.get(0);
                vBox.getChildren().add(bikeHandler.getContent());
                bikeHandlerArrayList.remove(bikeHandler);
            }

        }
    }

    public Stage getStage(){
        return stage;
    }
}
