package controller;

import model.Bike;
import model.Rental;
import utils.exception.EcoBikeException;
import utils.exception.InvalidRentalException;

import java.sql.SQLException;
import java.util.Date;

public interface RentalControllerInterface {
    /**
     *
     * @param rental
     * @return total rent fee based on rental (bike type and rent time duration)
     */
    long calculateRentFee(Rental rental);

    /**
     *
     * @param startTime the start time of rental
     * @return rent time duration, in second
     */
    long calculateRentTime(String startTime);

    /**
     *
     * @param rentalId
     * @return
     */
    void deleteRental(int rentalId) throws SQLException;

    /**
     *
     * @param bike
     * @param hour
     * @param min
     * @return
     * @throws InvalidRentalException
     */
    Integer calculateReserveFee(Bike bike, Integer hour, Integer min) throws InvalidRentalException;

    /**
     *
     * @param username
     * @param duration
     * @param startTime
     * @param bikeId
     * @param fee
     * @return 1 if success, else 0
     */
    int rentBike(String username, int duration, Date startTime, int bikeId, int fee);
}
