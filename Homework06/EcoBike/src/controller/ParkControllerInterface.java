package controller;

import model.Park;

import java.sql.SQLException;
import java.util.List;

public interface ParkControllerInterface {
    List<Park> getAllPark() throws SQLException;

    int getCurrentParkSlot(int parkId) throws SQLException;

    int getMaxParkSlot(int parkId) throws SQLException;
    
    boolean addPark(String name, String address, int capacity) throws SQLException;
}
