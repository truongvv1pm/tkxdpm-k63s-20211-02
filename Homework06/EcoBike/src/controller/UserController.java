package controller;

import model.Rental;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserController extends BaseController implements UserControllerInterface {
    @Override
    /**
     * See {@link UserControllerInterface#getUserRental(String)}
     */
    public Rental getUserRental(String username) {
        Rental rental = null;
        String query = "SELECT * FROM rental WHERE username = ?";
        try {
            PreparedStatement pstm = dbConnection.prepareStatement(query);
            pstm.setString(1, username);
            ResultSet results = pstm.executeQuery();
            while (results.next()) {
                rental = new Rental(
                        results.getInt(4),
                        results.getString(1),
                        results.getInt(2),
                        results.getString(3),
                        results.getInt(5),
                        results.getInt(6));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rental;
    }

    @Override
    public boolean validatePassword(String username, String password) {
        return false;
    }
}
