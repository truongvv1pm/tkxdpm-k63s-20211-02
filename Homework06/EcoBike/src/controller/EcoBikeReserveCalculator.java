package controller;

import model.Bike;
import utils.exception.InvalidRentalException;

public class EcoBikeReserveCalculator implements ReserveCalculator{
    @Override
    public int calculateReservingFee(Bike bike, Integer hour, Integer min) throws InvalidRentalException{
        if(bike == null){
            throw new InvalidRentalException("Bike is null");
        }
        String bikeType = bike.getType();
        if(bikeType == null || hour == null || min == null){
            throw new InvalidRentalException("Bike|hour|min cannot be null");
        }
        if(bikeType.equals("single")){
            return 400000;
        }
        if(bikeType.equals("twin")){
            return 550000;
        }
        if(bikeType.equals("single-electric")){
            return 700000;
        }
        throw new InvalidRentalException("No such bike type");
    }
}
