package controller;

import model.Bike;

public interface ReserveCalculator{
    /**
     * calculate reserving fee from bike, rental hour and minutes
     * @param bike
     * @param hour
     * @param min
     */
    int calculateReservingFee(Bike bike, Integer hour, Integer min);
}
