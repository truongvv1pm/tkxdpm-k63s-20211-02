package controller;

import model.Bike;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Bike;

public class BikeController extends BaseController implements BikeControllerInterface {
    @Override
    public String getBikeStatus(int bikeId) {
        String rs = "rented";
        try {
            PreparedStatement statement = dbConnection.prepareStatement("SELECT * FROM rental WHERE bike_id = ?;");
            statement.setInt(1, bikeId);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) {
                rs = "available";
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return rs;
    }

    @Override
    public Bike getBikeById(int bikeId) {
        Bike bike = null;
        try {
            PreparedStatement statement = dbConnection.prepareStatement("SELECT * FROM bike WHERE id = ?;");
            statement.setInt(1, bikeId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                bike = new Bike(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return bike;
    }

    @Override
    /**
     * See {@link BikeControllerInterface#updateBikePark(int, int)}
     */
    public void updateBikePark(int bikeId, int parkId) throws SQLException {
        PreparedStatement statement = dbConnection.prepareStatement("UPDATE bike SET park_id=? WHERE id=?");
        statement.setInt(1, parkId);
        statement.setInt(2, bikeId);
        statement.execute();
    }
}
