package controller;

import model.Park;
import model.Rental;
import utils.exception.EcoBikeException;
import utils.exception.InvalidBikeParkException;

import java.sql.SQLException;
import java.util.List;

public class ReturnBikeController extends BaseController {

    private final ParkControllerInterface parkController = new ParkController();
    private final UserControllerInterface userController = new UserController();
    private final BikeControllerInterface bikeController = new BikeController();
    private final RentalControllerInterface rentalController = new RentalController();

    public ReturnBikeController() {
        super();
    }

    /**
     * Fetch all parks
     *
     * @return all parks
     */
    public List<Park> getAllPark() throws SQLException {
        return parkController.getAllPark();
    }

    /**
     * Check if park is available or not. If not, throw new Exception
     *
     * @param parkId: the id corresponding
     */
    public void validateBikeVacancy(int parkId) throws SQLException {
        int currentSlot = parkController.getCurrentParkSlot(parkId);
        int maxSlot = parkController.getMaxParkSlot(parkId);

        if (currentSlot >= maxSlot)
            throw new InvalidBikeParkException();
    }

    /**
     * See {@link UserControllerInterface#getUserRental(String)}
     *
     * @param username
     * @return
     */
    public Rental getUserRental(String username) {
        return userController.getUserRental(username);
    }

    /**
     * See {@link RentalControllerInterface#calculateRentTime(String)}
     *
     * @param startTime
     * @return total renting time
     */
    public long calculateRentTime(String startTime) {
        return rentalController.calculateRentTime(startTime);
    }

    /**
     * See {@link RentalControllerInterface#calculateRentFee(Rental)}
     *
     * @param rental
     * @return
     */
    public long calculateRentFee(Rental rental) {
        return rentalController.calculateRentFee(rental);
    }

    /**
     * See {@link RentalControllerInterface#deleteRental(int)}
     *
     * @param rentalId
     */
    public void deleteRental(int rentalId) {
        try {
            rentalController.deleteRental(rentalId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new EcoBikeException("Đã có lỗi xảy ra!");
        }
    }

    /**
     * See {@link BikeControllerInterface#updateBikePark(int, int)}
     * @param bikeId
     * @param parkId
     */
    public void updateBikePark(int bikeId, int parkId) {
        try {
            bikeController.updateBikePark(bikeId, parkId);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new EcoBikeException("Đã có lỗi xảy ra!");
        }
    }
}
