package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Park;

public class ManageParkController extends BaseController{

	public ManageParkController() {
		super();
	}
	
	public List<Park> getAllPark(){
        List allPark = new ArrayList();
        try{
            allPark = new ParkController().getAllPark();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return allPark;
    }
}
