package controller;

import model.Park;
import utils.Configs;
import utils.exception.UserNotRentingException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HomeController extends BaseController{
    public HomeController(){
        super();
    }


    /**
     * return true if user is renting
     *
     * @param username
     * @return
     * @throws SQLException
     */
    public boolean checkUserRental(String username){
        return new UserController().getUserRental(username)!=null;
    }

    /**
     * @return
     */
    public List<Park> getAllPark(){
        List allPark = new ArrayList();
        try{
            allPark = new ParkController().getAllPark();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return allPark;
    }

}
