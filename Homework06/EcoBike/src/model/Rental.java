package model;

public class Rental{
    private int id;
    private String username;
    private int bikeId;
    private String startTime;
    private int duration;
    private int fee;

    public Rental(int id, String username, int bikeId, String startTime, int duration, int fee){
        this.id = id;
        this.username = username;
        this.bikeId = bikeId;
        this.startTime = startTime;
        this.duration = duration;
        this.fee = fee;
    }

    public Rental(String username, int bikeId, String startTime, int duration, int fee){
        this.username = username;
        this.bikeId = bikeId;
        this.startTime = startTime;
        this.duration = duration;
        this.fee = fee;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public int getBikeId(){
        return bikeId;
    }

    public void setBikeId(int bikeId){
        this.bikeId = bikeId;
    }

    public String getStartTime(){
        return startTime;
    }

    public void setStartTime(String startTime){
        this.startTime = startTime;
    }

    public int getDuration(){
        return duration;
    }

    public void setDuration(int duration){
        this.duration = duration;
    }

    public int getFee(){
        return fee;
    }

    public void setFee(int fee){
        this.fee = fee;
    }
}
