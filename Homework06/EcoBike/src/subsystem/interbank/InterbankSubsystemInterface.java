package subsystem.interbank;


import model.payment.PaymentCard;
import model.payment.PaymentTransaction;
import utils.exception.PaymentException;

public interface InterbankSubsystemInterface{

    PaymentTransaction payOrder(PaymentCard card, int amount, String contents) throws PaymentException;

    PaymentTransaction refund(PaymentCard card, int amount, String contents) throws PaymentException;
}
