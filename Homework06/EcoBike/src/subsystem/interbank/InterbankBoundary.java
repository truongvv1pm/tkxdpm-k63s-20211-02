package subsystem.interbank;

import utils.API;
import utils.exception.PaymentException;

public class InterbankBoundary{

    String query(String url, String data){
        String response;
        String token = "";
        try{
            response = API.patch(url, data, token);
        }catch(Exception e){
            // TODO Auto-generated catch block
            throw new PaymentException();
        }
        return response;
    }

}
