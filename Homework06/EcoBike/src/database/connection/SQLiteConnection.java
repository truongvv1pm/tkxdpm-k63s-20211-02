package database.connection;

import utils.Configs;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Provide methods to create connections to database
 */
public class SQLiteConnection implements DBConnection{
    private static Connection connection;

    public static Connection getConnection(){
        if(connection != null){
            return connection;
        }
        return connection = new SQLiteConnection().initConnection();
    }

    public static void main(String[] args){
        SQLiteConnection.getConnection();
    }

    @Override
    public Connection initConnection(){
        try{
            Class.forName("org.sqlite.JDBC");
            return DriverManager.getConnection(Configs.DATABASE_PATH);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
