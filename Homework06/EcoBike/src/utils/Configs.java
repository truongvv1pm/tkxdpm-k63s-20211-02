package utils;

public class Configs{
    public static final String DATABASE_PATH = "jdbc:sqlite:src/assets/db/eco_bike.db";
    public static final String PROCESS_TRANSACTION_URL = "https://ecopark-system-api.herokuapp.com/api/card/processTransaction";
    public static final String RESET_BALANCE_URL = "https://ecopark-system-api.herokuapp.com/api/card/reset-balance";

    public static String username = "";

    public static final String SUCCESS_ICON_PATH = "src/assets/images/success_icon.png";
    public static final String FAILURE_ICON_PATH = "src/assets/images/failure_icon.png";
    public static final String QUESTION_ICON_PATH = "src/assets/images/question_icon.png";

    public static final String SPLASH_SCREEN_PATH = "/views/fxml/splash.fxml";
    public static final String NOTIFICATION_POPUP_PATH = "/views/fxml/notification_popup.fxml";
    public static final String LOGIN_SCREEN_PATH = "/views/fxml/login.fxml";
    public static final String LOGIN_FOR_ADMIN_SCREEN_PATH = "/views/fxml/login_admin.fxml";
    public static final String HOME_SCREEN_PATH = "/views/fxml/home.fxml";
    public static final String ADMIN_SETTINGS_SCREEN_PATH = "/views/fxml/admin_settings.fxml";
    public static final String REGISTER_SCREEN_PATH = "/views/fxml/register.fxml";
    public static final String RETURN_BIKE_SCREEN_PATH = "/views/fxml/return_bike.fxml";
    public static final String RETURN_PARK_SCREEN_PATH = "/views/fxml/return_park.fxml";
    public static final String RETURN_BIKE_INVOICE_PATH = "/views/fxml/return_bike_invoice.fxml";

    public static final String PARK_HOME_ITEM = "/views/fxml/park_home.fxml";
    public static final String BIKE_RENTAL_DETAIL_ITEM = "/views/fxml/bike_rental_detail.fxml";
    public static final String PARK_RENTAL_DETAIL_SCREEN = "/views/fxml/park_rental_detail.fxml";

    public static final String MANAGE_BIKE_SCREEN_PATH = "/views/fxml/manage_bike.fxml";
    public static final String ADD_BIKE_SCREEN_PATH = "/views/fxml/BikeInfoForm.fxml";
    public static final String BIKE_DETAIL = "/views/fxml/bike_detail.fxml";
    public static final String MANAGE_PARK_SCREEN_PATH = "/views/fxml/manage_park.fxml";
    public static final String PARK_DETAIL_SCREEN_PATH = "/views/fxml/park_detail.fxml";
    public static final String ADD_PARK_FORM_SCREEN_PATH = "/views/fxml/add_park_form.fxml";
    public static final String CONFIRM_RENTING_DIALOG = "/views/fxml/confirm_renting.fxml";

    public static final String PAYMENT_FORM = "/views/fxml/payment_form.fxml";

}



