package utils;

public class Console{
    /**
     * console
     *
     * @param msg
     */
    public static void console(Object... msg){
        if(msg == null){
            return;
        }

        for(Object m : msg){
            System.out.print(m);
            System.out.print(" ");
        }
        System.out.println();
    }
}
