package utils.exception;

import java.io.IOException;

public class EcoBikeException extends RuntimeException {
    public EcoBikeException() {

    }

    public EcoBikeException(String message) {
        super(message);
    }

}
