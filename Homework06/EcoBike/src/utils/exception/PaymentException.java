package utils.exception;

public class PaymentException extends EcoBikeException{
    public PaymentException(String msg){
        super(msg);
    }

    public PaymentException(){
        super();
    }
}
