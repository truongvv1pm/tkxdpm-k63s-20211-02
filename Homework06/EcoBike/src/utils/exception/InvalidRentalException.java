package utils.exception;

public class InvalidRentalException extends EcoBikeException{

    public InvalidRentalException(String msg){
        super(msg);
    }
}
